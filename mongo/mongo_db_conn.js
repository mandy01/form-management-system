var mongoose=require('mongoose');
var db=mongoose.connection;
db.on('error',console.error);
db.once('open',function(){
  console.log('Connected Successully');
});
mongoose.connect("mongodb://localhost/test");

var myOwnSchema=new mongoose.Schema({
  title:String
});

var insertSchema=new mongoose.Schema({
  title:String
},{ strict: false });
var createFormSchema=new mongoose.Schema({
  formName:String,
  formDesc:String,
},{ strict: false });
var createInputFormRenderSchema=new mongoose.Schema({
  key:String,
  formId:String,
  type:String,
},{ strict: false });
var createInputFormSchema=new mongoose.Schema({
  inputPlaceholderName:String,
  inputLabelName:String,
  inputTypeName:String,
},{ strict: false });
var createSelectFormSchema=new mongoose.Schema({
  selectOptionName:String,
  selectLabelName:String,
},{ strict: false });
var createCheckboxFormSchema=new mongoose.Schema({
  checkboxLabelName:String,
},{ strict: false });
exports.CreateFormModel=mongoose.model('CreateFormColl',createFormSchema);
exports.CreateInputFormModel=mongoose.model('CreateInputFormColl',createInputFormSchema);
exports.CreateInputFormRenderModel=mongoose.model('CreateInputFormRenderColl',createInputFormRenderSchema);
exports.CreateSelectFormModel=mongoose.model('CreateSelectFormColl',createSelectFormSchema);
exports.CreateCheckboxFormModel=mongoose.model('CreateCheckboxFormColl',createCheckboxFormSchema);
exports.InsertSchemaModel=mongoose.model('InsertSchemaToDb',insertSchema);
exports.Docs=mongoose.model('Cms_Col',myOwnSchema);
console.log("Till here work properly");
