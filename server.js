var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/test';
var express = require('express');
var httprequest = require('request');
var app = express();
var path=require('path');
var util=require('util');
var cheerio=require('cheerio');
var fs=require('fs');
var mongo=require.main.require(path.resolve(__dirname,'mongo','mongo_db_conn.js'));
var constants=require.main.require(path.resolve(__dirname,'static_files','constants.js'));
var bodyParser=require('body-parser');
require(path.resolve(__dirname,'mongo','mongo_db_conn.js'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.resolve(__dirname,'static_files')));
app.use(bodyParser.json());


var getId=function(data){
  return data.replace(/\s/g, '');
};
var GLOBAL_FORM_ID;
app.post('/createForm',function(req,res){
  console.log(typeof(req.body));
  console.log('Creating Form with fields '+req.body);
  GLOBAL_FORM_ID=getId(req.body.formName);
  var insert_row=new mongo.CreateFormModel(req.body);
  insert_row.save(function(err, insert_row) {
    if (err) return console.error(err);
    console.dir(insert_row);
  });
});


app.post('/storeCurrentFormId',function(req,res){
GLOBAL_FORM_ID=req.body.form_id;
console.log(GLOBAL_FORM_ID);
});
function build_schema(GLOBAL_FORM_ID,input_data,select_data,checkbox_data){
  console.log("Hey There");
  var input_data_length=input_data.length;
  var select_data_length=select_data.length;
  var checkbox_data_length=checkbox_data.length;
  console.log("Input Length "+input_data.length);
  var obj={};
  var temp_schema={};
  var temp_form=[];
  var schema_obj_keyname;
  for(i=0;i<input_data_length;i++){
      schema_obj_keyname=Object.keys(input_data[i]._doc.schema_obj);
      temp_schema[schema_obj_keyname]=(input_data[i]._doc.schema_obj[schema_obj_keyname]);
      temp_form.push(input_data[i]._doc.render);
  }
  for(i=0;i<select_data_length;i++){
      schema_obj_keyname=Object.keys(select_data[i]._doc.schema_obj);
      temp_schema[schema_obj_keyname]=(select_data[i]._doc.schema_obj[schema_obj_keyname]);
      temp_form.push(select_data[i]._doc.render);
  } 
  for(i=0;i<checkbox_data_length;i++){
      schema_obj_keyname=Object.keys(checkbox_data[i]._doc.schema_obj);
      temp_schema[schema_obj_keyname]=(checkbox_data[i]._doc.schema_obj[schema_obj_keyname]);
      temp_form.push(checkbox_data[i]._doc.render);
  } 
      obj.schema=temp_schema;
      obj.render=temp_form;
      console.log("Finally Schema is ready "+obj);
      return obj;
}
app.get('/viewform',function(req,res){
  var schemaObj;
var input_query_obj=mongo.CreateInputFormModel.find({'formId':GLOBAL_FORM_ID},function(err,input_data){
  console.log("GLOBAL FORM ID "+GLOBAL_FORM_ID);
  console.log("Input Data "+input_data);
if(err){
  console.log("There is an error in retreiving the input objects");
}
else{
  var select_query_obj=mongo.CreateSelectFormModel.find({'formId':GLOBAL_FORM_ID},function(err,select_data){
    if(err){
      console.log("There is an error in retreiving the select objects");
    }
    else{
      var checkbox_query_obj=mongo.CreateCheckboxFormModel.find({'formId':GLOBAL_FORM_ID},function(err,checkbox_data){
        if(err){
          console.log("There is an error in retreiving the checkbox objects");
        }
        else{
          schemaObj=build_schema(GLOBAL_FORM_ID,input_data,select_data,checkbox_data);
          res.render(path.resolve(__dirname,'views','pages','formPreview.html'),{idata:JSON.stringify(schemaObj)});
        }
      });
    }
  });
}
});
});

app.get('/retreiveOptions',function(req,res){
  var temp_schema=constants.bd_schema_json;
  // var formId = req.query.form_id;
  // console.log("Hello Form Id is "+formId);
  var inputObj={};
  var selectObj={};
  var checkboxObj={};
  var inputSelectOptKeys=[];
  var input_query_obj=mongo.InsertSchemaModel.find(function(err,data){
    inputObj=data;
    if(err){
      console.log("There is an error");
    }
    else{
      console.log("Receiving the options "+data);
      res.send(temp_schema);
    }
  });
});

app.get('/totalForms',function(req,res){
  var formId = req.query.form_id;
  var formData={};
  var input_query_obj=mongo.CreateFormModel.find(function(err,data){
    formData=data;
    if(err){
      console.log("There is an error");
    }
    else{
      res.send(formData);
    }
  });
});

app.post('/deleteFormEntry',function(req,res){
  console.log(req.body.id);
  mongo.CreateFormModel.remove({ _id : req.body.id }, function (err) {
      console.log("Successfully Removed");
  });
});

app.post('/createInputForm',function(req,res){
  console.log(req.body);
  var insert_row=new mongo.CreateInputFormModel(req.body);
  insert_row.save(function(err, insert_row) {
    if (err) return console.error(err);
    console.dir(insert_row);
  });
});

app.post('/createCheckboxForm',function(req,res){
  console.log(req.body);
  // var key_name=getId(req.body.title);
  var insert_row=new mongo.CreateCheckboxFormModel(req.body);
  insert_row.save(function(err, insert_row) {
    if (err) return console.error(err);
    console.dir(insert_row);
  });
});

app.post('/createSelectForm',function(req,res){
  console.log(req.body);
  var insert_row=new mongo.CreateSelectFormModel(req.body);
  insert_row.save(function(err, insert_row) {
    if (err) return console.error(err);
    console.dir(insert_row);
  });
});

app.get('/fms', function(req, res) {
    res.render(path.resolve(__dirname,'views','pages','fms_frontend.html'));
});

app.post('/select',function(req,res){
  console.log(req.body);
  var select_field_object=req.body;
  var insert_field=new mongo.Docs({
    title: 'Mandeep',
    roll:'43'
  });
  insert_field.save(function(err, insert_field) {
    if (err) return console.error(err);
    console.dir(insert_field);
  })
});


app.get('/cms', function(req, res) {
    res.render(path.resolve(__dirname,'views','pages','cms_frontend.html'));
});


app.listen(8080);
console.log('8080 is the magic port');
