inputCounter=0;
selectCounter=0;
checkboxCounter=0;
optionCounter=0;
var unique_form_id;
var formNo;


var getId=function(data){
  return data.replace(/\s/g, '');
};

function createFormTools(unique_form_name){
  document.getElementById("leftContent").innerHTML="<div class='row'><div class='col-md-12'><button type='button' class='nextbutton1' id='addInput()' data-toggle='modal' data-target='#createInputFormDiv'>Create New Input Field</button></div></div>"+
  "<center><p style='color:black;'>or</p></center>"+
  "<div class='row'><div class='col-md-12'><select onchange='add_inputBox()' id='existing_input_fields'><option>Select From Existing Input fields</option></select></div></div>"+
  "<div class='row'><div class='col-md-12'><div id='space_inputButton' style='margin-bottom:20px;'></div></div></div>"+
  "<div class='row'><div class='col-md-12'><button type='button' id='addSelect()' class='nextbutton1' data-toggle='modal' data-target='#createSelectFormDiv'>Create New Select Field</button></div></div>"+
  "<center><p style='color:black;'>or</p></center>"+
  "<div class='row'><div class='col-md-12'><select onchange='add_selectBox()' id='existing_select_fields'><option>Select From Existing Select fields</option></select></div></div>"+
  "<div class='row'><div class='col-md-12'><div id='space_selectButton' style='margin-bottom:20px;'></div></div></div>"+
  "<div class='row'><div class='col-md-12'><button type='button' id='addCheckbox()' class='nextbutton1' data-toggle='modal' data-target='#createCheckboxFormDiv'>Create New Checkbox</button></div></div>"+
  "<center><p style='color:black;'>or</p></center>"+
  "<div class='row'><div class='col-md-12'><select onchange='add_checkboxBox()' id='existing_checkbox_fields'><option>Select From Existing Checkbox</option></select></div></div>"+
  "<div class='row'><div class='col-md-12'><div id='space_buttButton'></div></div></div>";
  document.getElementById("rightContent").innerHTML='<button class="previewBtn btn-success col-md-4" onclick="previewForm(\''+unique_form_name+'\')">Preview Form</button>';
  var input_key_arr=[];
  var temp_string="";
  var temp_obj={};
  var selectBoxForInput = document.getElementById("existing_input_fields");
  var selectBoxForSelect = document.getElementById("existing_select_fields");
  var selectBoxForCheckbox = document.getElementById("existing_checkbox_fields");
  $.ajax({
    type:"GET",
    url:"http://localhost:8080/retreiveOptions",
    // data:{form_id:formId},
    success:function(data){
      console.log("Here is the option data "+data);
      options_data=data;
      input_key_arr=Object.keys(options_data);
      console.log(input_key_arr[0]);
      for(a=0;a<input_key_arr.length;a++){
        temp_string=input_key_arr[a].toString();
        console.log(options_data[temp_string].enum);
        if(options_data[temp_string].enum==undefined){
          var option = document.createElement("option");
          option.text = options_data[temp_string].title;
          option.value =options_data[temp_string].title;
          selectBoxForInput.appendChild(option);
        }
        else{
          var option = document.createElement("option");
          option.text = options_data[temp_string].title;
          option.value =options_data[temp_string].title;
          selectBoxForSelect.appendChild(option);
        }
      }
    }
  });
}


function sendCreateForm(){
  var raw_data = $('#createForm').serializeArray();
  var form_data = {};
  var $this = $('#createForm');
  $.each(raw_data, function(){
    form_data[this.name] = this.value;
  });
  unique_form_id=getId(form_data.formName);
  console.log(form_data);
  $.ajax({
    type:"POST",
    url:"http://localhost:8080/createForm",
    data: JSON.stringify(form_data),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success:function(data){
      console.log("Inside the success function");
      console.log(data);
    }
  });
  $("#createFormDiv").modal('hide');
  createFormTools(form_data.formName);
}

function sendInputForm(){
  var raw_data = $('#createInputForm').serializeArray();
  var form_data = {};
  var form_render_data={};
  var final_data={};
  var render_data={};
  var schema_data={};
  var $this = $('#createInputForm');
  $.each(raw_data, function(){
    if(this.name=="required"){
      if(this.value=="on"){
        form_data[this.name]="true";
      }
      else{
        form_data[this.name]="false";
      }
    }
    else{
    form_data[this.name] = this.value;
    }
  });
  var key_name=getId(form_data.title);
  var render_key_name=getId(form_data.title)+"_render";
  console.log("key name "+key_name);
  form_data.elementType="text";

  schema_data[key_name]=form_data;

  render_data.key=key_name;
  render_data.type=form_data.type;
  final_data.schema_obj=schema_data;

  final_data.render=render_data;
  final_data.formId=unique_form_id.toString();
  console.log(final_data);
  $.ajax({
    type:"POST",
    url:"http://localhost:8080/createInputForm",
    data:JSON.stringify(final_data),
    contentType: "application/json; charset=utf-8",
    success:function(data){
      console.log("Inside the success function");
      console.log(data);
    }
  });
  $("#createInputFormDiv").modal('hide');
}

function sendSelectForm(){
  var enum_arr=[];
  var raw_data = $('#createSelectForm').serializeArray();
  var form_data = {};
  var form_render_data={};
  var final_data={};
  var render_data={};
  var schema_data={};
  var $this = $('#createSelectForm');
  $.each(raw_data, function(){
    if(this.name=="required"){
      if(this.value=="on"){
        form_data[this.name]="true";
      }
      else{
        form_data[this.name]="false";
      }
      if((this.name).slice(0,4)=="enum"){
        enum_arr.push(this.value);
      }
      else{
      form_data[this.name] = this.value;
      }
  }
  else{
     if((this.name).slice(0,4)=="enum"){
      enum_arr.push(this.value);
    }
    else{
    form_data[this.name] = this.value;
    }   
  }
  });
  form_data["enum"]=enum_arr;

  var key_name=getId(form_data.title);
  var render_key_name=getId(form_data.title)+"_render";
  form_data.elementType="text";

  schema_data[key_name]=form_data;

  render_data.key=key_name;
  render_data.type=form_data.elementType;
  final_data.schema_obj=schema_data;

  final_data.render=render_data;
  final_data.formId=unique_form_id.toString();
  $.ajax({
    type:"POST",
    url:"http://localhost:8080/createSelectForm",
    data: JSON.stringify(final_data),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success:function(data){
      console.log("Inside the success function");
      console.log(data);
    }
  });
  $("#createSelectFormDiv").modal('hide');
}

function sendCheckboxForm(){
  var raw_data=$('#createCheckboxForm').serializeArray();
  var form_data = {};
  var form_render_data={};
  var final_data={};
  var render_data={};
  var schema_data={};
  var $this=$('#createCheckboxForm');
  $.each(raw_data, function(){
    if(this.name=="checkboxRequired"){
      if(this.value=="on"){
        form_data[this.name]="true";
      }
      else{
        form_data[this.name]="false";
      }
  }
  else{
    if(this.name=="checkboxLabelName"){
      form_data.title=this.value;
    }
    else{
    form_data[this.name] = this.value; 
    }
  }
  });

  var key_name=getId(form_data.title);
  var render_key_name=getId(form_data.title)+"_render";
  form_data.elementType="boolean";
  form_data.type="boolean";
  schema_data[key_name]=form_data;

  render_data.key=key_name;
  render_data.inlinetitle=form_data.title;
  final_data.schema_obj=schema_data;

  final_data.render=render_data;
  final_data.formId=unique_form_id.toString();

  $.ajax({
    type:"POST",
    url:"http://localhost:8080/createCheckboxForm",
    data: JSON.stringify(final_data),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success:function(data){
      console.log("Inside the success function");
      console.log(data);
    }
  });
  $("#createCheckboxFormDiv").modal('hide');
}


function duplicateInputForm(){
  ++(inputCounter);
  document.getElementById("inputFormContainer").innerHTML+='</br><label>Enter Placeholder Name</label>'+
                  '</br>'+
                  '<input type="text" name="inputPlaceholderName'+(inputCounter)+'"/>'+
                  '</br>'+
                  '<label>Enter Label Name</label>'+
                  '</br>'+
                  '<input type="text" name="inputLabelName'+(inputCounter)+'"/>'+
                  '</br>'+
                  '<label>Select Type</label>'+
                  '</br>'+
                  '<select name="inputTypeName'+(inputCounter)+'">'+
                    '<option value="text">Text</option>'+
                    '<option value="number">Number</option>'+
                    '<option value="password">Password</option>'+
                    '<option value="email">Email</option>'+
                    '<option value="date">Date</option>'+
                  '</select>'+
                  '</br>'+
                  '<label>Required</label>'+
                  '<input type="checkbox" name="inputRequired'+(inputCounter)+'"/>'+
                  '</br>';
}
function addOption(){
  ++(optionCounter);
  document.getElementById('optionSelectContainer').innerHTML+='<input id="select_option_'+(optionCounter)+'" type="text" name="enum_'+(optionCounter)+'"/></br>';
}
function duplicateSelectForm(){
  ++(selectCounter);
  document.getElementById('inputSelectContainer').innerHTML+='<label>Enter Options</label>'+
                      '</br>'+
                      '<div id="optionSelectContainer">'+
                        '<input type="text" name="selectOptionName'+(selectCounter)+'"/>'+
                        '</br>'+
                      '</div>'+
                      '<button type="button" onclick="addOption()">Add More Options</button>'+
                      '</br>'+
                      '<label>Enter Label Name</label>'+
                      '</br>'+
                      '<input type="text" name="selectLabelName'+(selectCounter)+'"/>'+
                      '</br>'+
                      '<label>Required</label>'+
                      '<input type="checkbox" name="selectRequired'+(selectCounter)+'"/>'+
                      '</br>';
}

function duplicateCheckboxForm(){
  ++(checkboxCounter);
  document.getElementById('inputCheckboxContainer').innerHTML+='</hr><label>Enter Label</label>'+
                      '</br>'+
                        '<input type="text" name="checkboxLabelName'+(checkboxCounter)+'"/>'+
                        '</br>'+
                        '<label>Required</label>'+
                        '<input type="checkbox" name="checkboxRequired'+(checkboxCounter)+'"/>'+
                        '</br>';
}

function add_inputBox(){
document.getElementById("space_inputButton").innerHTML="<button style='margin-left:10px;' class='nextbutton1'>Add Input Field</button>";
}
function add_selectBox(){
document.getElementById("space_selectButton").innerHTML="<button class='nextbutton1'>Add Select Field</button>";
}
function add_checkboxBox(){
document.getElementById("space_checkboxButton").innerHTML="<button class='nextbutton1'>Add Checkbox Field</button>";
}
function add_buttonBox(){
document.getElementById("space_buttButton").innerHTML="<button class='nextbutton1'>Add Button Field</button>";
}
function previewForm(form_name){
  var form_id=getId(form_name);
  console.log("Hereee is your form id "+form_id);
  $.ajax({
    type:"POST",
    url:"http://localhost:8080/storeCurrentFormId",
    data: JSON.stringify({"form_id":form_id}),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success:function(data){
    }
  });
  window.location="http://localhost:8080/viewform";
}
