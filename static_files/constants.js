exports.bd_schema_json={
   company_name:{
      type:"string",
      title:"Company Name",
      placeholder:"Enter your name",
      required:true
   },
   pdt_type:{
      type:"string",
      title:"Select Product Type",
      enum:[
         "Loan Against Property",
         "Lease Rental Discounting",
      ],
      required:true
   },
   contact_type:{
      type:"string",
      title:"Contact type",
      placeholder:"Eg. Promoter/CA/Lawyer/PA",
      required:true
   },
   name:{
      type:"string",
      title:"Name",
      placeholder:"Enter Name here",
      required:true
   },
   phone_number:{
      type:"number",
      title:"Phone Number",
      required:true
   },
   email:{
      type:"string",
      title:"Email Address",
      placeholder:"Enter email here",
      required:true
   },
   send_wel_mail:{
      type:"array",
      items:{
         enum:[
            'chckboxes'
         ]
      }
   },
   address:{
      type:"string",
      title:"Address",
      placeholder:"Enter Street here"
   },
   city:{
      type:"string",
      title:"City",
      placeholder:"Enter City here"
   },
   state:{
      type:"string",
      title:"State",
      placeholder:"Enter State here"
   },
   pin:{
      type:"number",
      title:"PinCode"
   },
   originated:{
      type:"string",
      title:"Originated By",
      enum:[
         "Please Select",
         "Mandeep Singh"
      ]
   },
   assigned_to:{
      type:"string",
      title:"Assigned To",
      enum:[
         "Please Select",
         "Mandeep Singh"
      ],
      required:true
   },
   industry_type:{
      type:"string",
      title:"Select Industry Type",
      enum:[
         "Please Select",
         "Wood &amp; Wood Products"
      ],
      required:true
   },
   expected_loan_amount:{
      type:"string",
      title:"Expected Loan Amount",
      placeholder:"Enter Loan Amount"
   },
   credit_analyst:{
      type:"string",
      title:"Credit Analyst",
      enum:[
         "Please Select",
         "Kabir Narain"
      ],
      required:true
   },
   next_call_date:{
      type:"date",
      title:"Next Call Date",
      placeholder:"Next Call Date",
      required:true
   },
   channel:{
      type:"string",
      title:"Channel",
      enum:[
         "Please Select",
         "ZAP"
      ],
      required:true
   },
   initial_background:{
      type:"string",
      title:"Initial Background",
      placeholder:"Zouk BD Comment"
   },
   call_completion_date:{
     type:"date",
     title:"Call Completion Date",
     required:true
  },
  call_status:{
     type:"string",
     title:"Call Status",
     enum:[
        "Select Call Status"
     ]
  },
  call_results:{
     type:"string",
     title:"Call Results",
     enum:[
        "Select Call Results"
     ]
  },
  next_call_date:{
     type:"date",
     title:"Next Call Date",
     required:true
  },
  action:{
     type:"string",
     title:"Action"
  },
  comments:{
   type:"string",
   title:"Comments"
  },
  loan_amount_required:{
    type:"file",
    title:"Loan Amount Required"
  },
  loan_amount_required:{
   type:"string",
   title:"Loan Amount Required"
  },
  tenure:{
     type:"string",
     title:"Tenure"
  },
  expected_rate_of_interest:{
     type:"string",
     title:"Expected rate of interest"
  },
  expected_fee:{
     type:"string",
     title:"Expected Fee"
  },
  comfortable_obligation_levels:{
     type:"string",
     title:"Comfortable Obligation Levels"
  },
  security_offered:{
     type:"string",
     title:"Security offered"
  },
  how_old_business:{
     type:"string",
     title:"How old is this business?"
  },
  revenue_model:{
     type:"string",
     title:"What is the revenue model?"
  },
  turnover:{
     type:"string",
     title:"What is your last 2 years turnover"
  },
  gross_margin:{
     type:"string",
     title:"What are gross margins in this business"
  },
  break_event:{
     type:"string",
     title:"Have we achieved break even?"
  },
  monthly_sales:{
     type:"string",
     title:"Current level of monthly sales"
  },
  monthly_obligations:{
     type:"string",
     title:"Current monthly obligations if any"
  },
  working_capital:{
     type:"string",
     title:"Working capital cycle in days"
  },
  fund_arrangement:{
     type:"string",
     title:"Amount and banker for existing fund arrangement"
  },
  education_profile:{
     type:"string",
     title:"Education profile of promoters"
  },
  promoters:{
     type:"string",
     title:"Exp of promoters in this business & otherwise"
  },
  net_worth:{
     type:"string",
     title:"Net worth of promoters (Tangible assets owned)"
  },
  history_loan_repayment:{
     type:"string",
     title:"Any history of loan repayment"
  }
}
