function activateCreateFormModal(ht) {
    form_modal_var = document.createElement('div');
    form_modal_var.style.width = '400px';
    if(typeof(ht)==undefined){
    form_modal_var.style.height = '400px';
    }
    else{
    form_modal_var.style.height = ht;
    }
    form_modal_var.style.margin = '100px auto';
    form_modal_var.style.backgroundColor = '#eeeeee';
    form_modal_var.style.padding='20px';
    form_modal_var.innerHTML=$('#create_form_modal_div').html();
    mui.overlay('on', form_modal_var);

  }
function on_next_create_form(){
      mui.overlay('off', form_modal_var);
        document.getElementById("tool_bar").innerHTML='<div id="add_elements">'+
              '<center><button id="add_input_box" onclick="activateInputModal(400)" style="width:100%;" class="mui-btn mui-btn mui-btn--raised mui--z2" >Add Input Field</button></center>'+
              '<center><button id="add_select_box" onclick="activateSelectModal(400)" style="width:100%;" data-toggle="modal" data-target="#select_modal" class="mui-btn mui-btn mui-btn--raised mui--z2" >Add Select Field</button></center>'+
              '<center><button id="add_checkbox" onclick="activateCheckboxModal(300)" style="width:100%;" data-toggle="modal" data-target="#checkbox_modal" class="mui-btn mui-btn mui-btn--raised mui--z2">Add Checkbox</button></center>'+
              '<center><button id="add_button" data-toggle="modal" data-target="#button_modal" style="width:100%;" class="mui-btn mui-btn mui-btn--raised mui--z2">Add Button</button></center>'+
              '</div>';
}
alt_color_select=0;
function activateInputModal(ht) {
    input_modal_var = document.createElement('div');
    input_modal_var.style.width = '400px';
    if(typeof(ht)==undefined){
    input_modal_var.style.height = '400px';
    }
    else{
    input_modal_var.style.height = ht;
    }
    input_modal_var.style.margin = '100px auto';
    input_modal_var.style.backgroundColor = '#eeeeee';
    input_modal_var.style.padding='20px';
    input_modal_var.innerHTML=$('#input_modal_div').html();
    mui.overlay('on', input_modal_var);

  }
  function activateSelectModal(ht) {
      // initialize modal element
      select_modal_var = document.createElement('div');
      select_modal_var.style.width = '400px';
      if(typeof(ht)==undefined){
      select_modal_var.style.height = '400px';
      }
      else{
      select_modal_var.style.height = ht;
      }
      select_modal_var.style.margin = '100px auto';
      select_modal_var.style.backgroundColor = '#fff';
      select_modal_var.style.padding='20px';
      select_modal_var.innerHTML=$('#select_modal_div').html();
      // show modal
      mui.overlay('on', select_modal_var);

    }

    function activateCheckboxModal(ht) {
        // initialize modal element
        checkbox_modal_var = document.createElement('div');
        checkbox_modal_var.style.width = '400px';
        if(typeof(ht)==undefined){
        checkbox_modal_var.style.height = '300px';
        }
        else{
        checkbox_modal_var.style.height = ht;
        }
        checkbox_modal_var.style.margin = '100px auto';
        checkbox_modal_var.style.backgroundColor = '#eeeeee';
        checkbox_modal_var.style.padding='20px';
        checkbox_modal_var.innerHTML=$('#checkbox_modal_div').html();
        // show modal
        mui.overlay('on', checkbox_modal_var);

      }

      i=0;
function input_modal_container_duplicate(){
      ++i;
      //input_modal_var.innerHTML+='<div class="mui-row">'+
      document.getElementById("input_modal_container").innerHTML+='<hr/><div class="mui-row">'+
      '<div class="mui-col-md-12">'+
      '<div class="mui-textfield mui-textfield--float-label">'+
      '<input type="text" name="input_label_field'+(i)+'" id="input_label_field'+(i)+'" required/>'+
      '<label>Enter Label</label>'+
      '</div></div>'+
      '</div>'+
      '<div class="mui-row">'+
      '<div class="mui-col-md-12">'+
      '<div class="mui-textfield mui-textfield--float-label">'+
      '<input type="text" name="input_placeholder_field'+(i)+'" id="input_placeholder_field'+(i)+'" required/>'+
      '<label>Enter Placeholder</label>'+
      '</div></div>'+
      '</div>'+
      '<div class="mui-row">'+
      '<div class="mui-col-md-12">'+
      '<div class="mui-select">'+
        '<select name="input_type_selector'+(i)+'" id="input_type_selector'+(i)+'">'+
          '<option value="text">Text</option>'+
          '<option value="number">Number</option>'+
          '<option value="password">Password</option>'+
          '<option value="email">Email</option>'+
          '<option value="date">Date</option>'+
        '</select>'+
      '</div></div>'+
      '</div>';
      var prev_ht=parseInt(input_modal_var.style.height);
      var new_ht=prev_ht+300;
      input_modal_var.style.height= new_ht+'px';
      activateInputModal(new_ht+'px');
}
select_counter=0;
function select_modal_container_duplicate(){
  ++(select_counter);
      document.getElementById("select_modal_container").innerHTML+='<hr/><div class="mui-row">'+
          '<div class="mui-col-md-12">'+
          '<div class="mui-textfield mui-textfield--float-label" id="options_instance">'+
            '<input type="text" id="select_options_field'+(select_counter)+'" name="select_options_field'+(select_counter)+'"/>'+
            '<label for="select_options_field'+(select_counter)+'">Enter Options</label>'+
          '</div></div>'+
          '</div>'+
          '<div class="mui-row">'+
          '<div class="mui-col-md-12">'+
            '<button type="button" class="mui-btn mui-btn--raised" onclick="add_more_options()" style="background-color:#eeeeee;">Add More Options</button>'+
          '</div></div>'+
          '<div class="mui-row">'+
          '<div class="mui-col-md-12">'+
          '<div class="mui-textfield mui-textfield--float-label">'+
            '<input type="text" id="select_label_field'+(select_counter)+'" name="select_label_field'+(select_counter)+'"/>'+
            '<label for="select_label_field'+(select_counter)+'">Enter Label</label>'+
          '</div></div>'+
          '</div>';
      var prev_ht=parseInt(select_modal_var.style.height);
      var new_ht=prev_ht+300;
      select_modal_var.style.height= new_ht+'px';
      activateSelectModal(new_ht+'px');

}
checkbox_counter=0;
function checkbox_modal_container_duplicate(){
      //input_modal_var.innerHTML+='<div class="mui-row">'+
      document.getElementById("checkbox_modal_container").innerHTML+='<hr/><div class="mui-row">'+
          '<div class="mui-col-md-12">'+
          '<div class="mui-textfield mui-textfield--float-label" id="options_instance">'+
            '<input type="text" id="checkbox_label_field'+(checkbox_counter)+'" name="checkbox_label_field'+(checkbox_counter)+'"/>'+
            '<label for="checkbox_label_field'+(checkbox_counter)+'">Enter Label</label>'+
          '</div></div>'+
          '</div>';
      var prev_ht=parseInt(checkbox_modal_var.style.height);
      var new_ht=prev_ht+300;
      checkbox_modal_var.style.height= new_ht+'px';
      activateCheckboxModal(new_ht+'px');
}
function serializeInputModalData(){

        console.log("Serializing the input form data");
        var raw_data = $('#input_modal_form').serializeArray();
        var form_data = {};
        var $this = $('#input_modal_form');
        $.each(raw_data, function(){
          form_data[this.name] = this.value;
        });
        console.log(form_data);
}

function serializeCheckboxModalData(){
        console.log("Serializing the input form data");
        var raw_data = $('#checkbox_modal_form').serializeArray();
        var form_data = {};
        var $this = $('#select_modal_form');
        $.each(raw_data, function(){
          form_data[this.name] = this.value;
        });
        console.log(form_data);
}
