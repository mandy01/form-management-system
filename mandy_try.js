var httprequest = require('request');
var MongoClient = require('mongodb').MongoClient;
var Consts = require.main.require('./app/server/constants.js');

var url = Consts.config.Mongo_url;
var db, logs, visitors;

exports.main = function(callback){
  MongoClient.connect(url, function (err, database) {
    if (err) {
      return console.log('FATAL:', err);
    } else {
      db = database;
      logs = db.collection('zlogs');
      visitors = db.collection('visitors');
    }
    callback(db);
  });
};

exports.getDB = function(callback){
  return db;
};

exports.log = function(ob){
  if(ob !== null && typeof ob === 'object') logOb(ob);
  else logOb({msg: ob});
};

exports.logActivity = function(r, a, ob){
  logAct(r,a,ob);
};

function logOb(ob){
  ob.unix = Date.now();
  logs.insert(ob, function(err, rows){
    if(err) return console.log('ERR mongo.js:', __lineNumber, err);
  });
}

/*
activity: {page: page, action: action}
ob: {uuid: uuid}
*/

function logAct(request, activity, ob){
  if(!ob) ob = {};
  var keyz = Object.keys(ob);
  for (var i = 0; i < keyz.length; i++) activity[keyz[i]] = ob[keyz[i]];
  var ip;
  if(request.ip && request.ip.length > 0) ip = request.ip;
  else ip = 'Anon';
  logOb({
    msg: 'Activity log.',
    ip: ip,
    activity: activity
  });
  activity.unix = Date.now();
  visitors.find({ip: ip}).toArray(function(err, rows){
    if(err) return mError(__lineNumber, err);
    if(rows.length == 0) {
      httprequest('http://ip-api.com/json/'+ip, function(err, response, data){
        if(err) return mError(__lineNumber, err);
	      data = JSON.parse(data);
        var o = {};
        o.ip = ip;
        o.ip_location = data.city+', '+data.country;
        o.isp = data.isp;

        o.activity = [activity];
        visitors.insert(o, function(err, rows){
          if(err) return mError(__lineNumber, err);
        });
      });
    } else {
      visitors.update({ip: ip}, {$push: {activity: activity}}, {upsert: true}, function(err, rows){
        if(err) return mError(__lineNumber, err);
      });
    }
  });
}

function mError(line, err){
  return console.log('ERROR mongo.js :', line, 'err', err);
}
