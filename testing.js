var fs=require('fs');
var path=require('path');
var ReaderStream=fs.createReadStream(path.resolve(__dirname,'static_files','bd_schema.json'));
ReaderStream.setEncoding('UTF8');
var data={};
ReaderStream.on('data',function(chunk){
  data+=chunk;
});
ReaderStream.on('end',function(){
  console.log(data);
});
ReaderStream.on('error',function(err){
  console.log(err.stack);
});
console.log("Program Ended");
